
# DELETING COMMENTS
# Deleting comments should be done between the header of the macro definition
# (e.g. `macro MacroName 34) and the footer of the definition i.e. `endmacro
/^[     ]*`macro[   ]+[A-Za-z][A-Za-z0-9_~]{1,63}[  ]*/, /^[  ]*`endmacro[   ]+/ {
	
	# Delete multiline comments noting that they could be inlined
	/`!.*/, /.*!`/ {

		# On the line where the comment begins, delete just the comment while 
		# preserving the code if there's any
		/[^`]*`!.*/ s/[     ]*`!.*//
		s/.*!`[ 	]*//

		# Read in the next line
		:FetchNextLine
		n

		# If its the last line of the comment, GoTo the label DeleteLast
		# /!`/b DeleteLast
		
		# If its not the last line of the comment, delete the whole line
		d
		b FetchNextLine

		# Delete the comment part of the last line of comment while preserving
		# code if present
		:DeleteLast
		s/!`//
	}

	# Delete multiline comments where the comments start and end on the same line
	# Multiline comments begin with `! and end with !`
	/[^`]*`![^!]*!`/ s/`![^!]*!`//

	# Delete lines that start with `~. These are single line comments
	/[^`]*`~/ s/`~.*//

	# Concatenate lines that end with space and backtick with the next line.
	# Space and backtick ( `) marks line continuation
	/ `$/ {

		:loop
		N
		s/ `\n//
		/ `$/b loop
	}
}